package com.project.friendsapp;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;


public class MainActivity extends Activity {
	ProgressDialog pd;
	Context context;
	Button scan_save;
	public static final String TAG ="com.project.friendsapp";
//	ParseInstallation parseInstall;
	List<ParseObject> applist;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ParseAnalytics.trackAppOpened(getIntent());
		
		context=this;
		applist = new ArrayList<ParseObject>();
		scan_save = (Button) findViewById(R.id.scan_save_btn);
		scan_save.setOnClickListener(scan_saveButtonListener);
	}
	public OnClickListener scan_saveButtonListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			v.setEnabled(false);
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
				
				@Override
				protected void onPreExecute() {
					pd = new ProgressDialog(context);
					pd.setTitle("Processing...");
					pd.setMessage("Please wait. .... \nSaving names of your apps to cloud");
					pd.setCancelable(false);
					pd.setIndeterminate(true);
					pd.show();
				}

				@Override
				protected Void doInBackground(Void... arg0) {
					final PackageManager pm = getPackageManager();
					
					List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
					for (ApplicationInfo packageInfo : packages) {
						ParseObject n = new ParseObject("AppList");
						
						n.put("name",android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID));
						n.put("App",(String)pm.getApplicationLabel(packageInfo));
						applist.add(n);
					    Log.d(TAG, "Installed package :" + (String)pm.getApplicationLabel(packageInfo));
					   // Log.d(TAG, "Source dir : " + packageInfo.sourceDir);
					    
					    }
				ParseObject.saveAllInBackground(applist, new SaveCallback() {
					
					@Override
					public void done(ParseException arg0) {
						if(arg0==null){
							
						}
						else{
							
							
						}
						// TODO Auto-generated method stub
						
					}
				});
					return null;
				}
				@Override
				protected void onPostExecute(Void result) {
					if (pd!=null) {
						pd.dismiss();
						Intent intent= new Intent(MainActivity.this,NFCActivity.class);
						startActivity(intent);
						MainActivity.this.finish();
					}
				}
			};
			task.execute((Void[])null);
			
			
			
		}
	};
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
