package com.project.friendsapp;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class NFCActivity extends Activity implements CreateNdefMessageCallback,OnNdefPushCompleteCallback {

	NfcAdapter mNfcAdapter;
	ParseQuery<ParseObject> query= ParseQuery.getQuery("AppList");
	
	ArrayList<String>mylist;
	ArrayList<String>temp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nfc);
		// Show the Up button in the action bar.
		setupActionBar();
		
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mNfcAdapter.setNdefPushMessageCallback(this, this);
		mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
		final PackageManager pm = getPackageManager();
		temp= new ArrayList<String>();
		mylist=new ArrayList<String>();
		ParseAnalytics.trackAppOpened(getIntent());
		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		for(ApplicationInfo packageInfo: packages){
			if((String)pm.getApplicationLabel(packageInfo)!=null){
				mylist.add((String)pm.getApplicationLabel(packageInfo));
				
			}
		}
		
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nfc, menu);
		return true;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public NdefMessage createNdefMessage(NfcEvent event) {
		// TODO Auto-generated method stub
		 NdefMessage msg = new NdefMessage(
	                new NdefRecord[] {NdefRecord.createMime("text/plain",android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID).getBytes())
	                		
	                });
		return msg;
	}
	 @Override
	    public void onResume() {
	        super.onResume();
	        // Check to see that the Activity started due to an Android Beam
	        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
	           processIntent(getIntent());
	        }
	    }
	  @Override
	    public void onNewIntent(Intent intent) {
	        // onResume gets called after this to handle the intent
	        setIntent(intent);
	    }
	  void processIntent(Intent intent) {
	        //listview set
		  
	        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
	                NfcAdapter.EXTRA_NDEF_MESSAGES);
	        // only one message sent during the beam
	        NdefMessage msg = (NdefMessage) rawMsgs[0];
	        // record 0 contains the MIME type, record 1 is the AAR, if present
	        String other_id = new String(msg.getRecords()[0].getPayload());
	        Log.d("HERE",other_id);
	        Toast.makeText(this, new String(msg.getRecords()[0].getPayload()), Toast.LENGTH_LONG).show();

	        query.whereEqualTo("name", other_id);
	        query.findInBackground(new FindCallback<ParseObject>() {
				
				@Override
				public void done(List<ParseObject> listofapp, ParseException arg1) {
					
					if(arg1==null){
						if(listofapp!=null){
							for(ParseObject app:listofapp){
								temp.add((String) app.get("App"));
							}
						}
						temp.removeAll(mylist);
						Intent intent = new Intent(NFCActivity.this,ApplistActivity.class);
						intent.putStringArrayListExtra(ApplistActivity.EXTRA_APPS, temp);
						startActivity(intent);
						NFCActivity.this.finish();
						
					}
					
					
				}
			});
	       // textView.setText(new String(msg.getRecords()[0].getPayload()));
	    }

	@Override
	public void onNdefPushComplete(NfcEvent event) {
		// TODO Auto-generated method stub
		Log.d("tag","complete");
		
	}

}
